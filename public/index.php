<?php
include('../app/config.ini.php');
include('../app/login.lib.php');
if ($_SERVER['REQUEST_METHOD']=='POST') { // Miramos si hay datos en el login
	// include('../assets/php/config.ini.php'); //incluimos configuración
	//include('../assets/php/login.lib.php'); //incluimos las funciones
	//verificamos el usuario y contraseña mandados
	if (login($_POST['username'],$_POST['password'])) {
		//echo "<div style='display:block;'id='MensajeError' class='mensaje ok'name='Login correcte' text='Usuari i contrasenya correctes'><a href='formInscripcio.php'><input type='button'value='acceptar';' class='boton_cerrar' ></a></div>";
		if ($_SESSION['USUARIO']['rol'] == 1){
			$correu = "davevudu@gmail.com";
			$asunto = "Inici sessió ".$_POST['username'];
			$ip = getRealIP();
			$hora= date ("h:i:s");
			$fecha= date ("j/n/Y");
			$cos = "L'usuari : ".$_POST['username']." ha iniciat sessio amb IP ".$ip." en fecha : ".$fecha." y hora : ".$hora;
			$resultado = mail($correu,$asunto,$cos);
			if ($resultado) {
				echo "Correo enviado correctamente";
			}else{
				echo "El correo no se ha enviado correctamente";
			}
			$file = fopen("../app/sessions.txt", "a");
			fwrite($file, $cos . PHP_EOL);
			fclose($file);
		}
		header('location:formInscripcio.php');
		// die();
	} else {
		echo "<div style='display:block;'id='MensajeError' class='mensaje error'name='Login incorrecte' text='Usuari i contrasenya incorrectes'><a href='index.php'><input type='button'value='acceptar';' class='boton_cerrar' ></a></div>";
		$mensaje='Usuario o contraseña incorrecto.';
	}
}
?>
<!DOCTYPE html>

<head>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3fARb25zx2ECLD3pD7VaxbIR3sKEKAB2";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<title>Inscripció Balles</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8"> 
<link rel="shortcut icon" type="image/x-icon" href="../app/images/logoBalles.ico" />
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.min.css">
<script src="../app/js/jquery-1.10.2.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../app/js/scripts.js"></script>
<link rel="stylesheet" href="../app/css/estil.css" type="text/css" media="all" />
<link rel="stylesheet" href="../app/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../app/bootstrap/css/signin.css">
<script type="text/javascript">// <![CDATA[
var navegador = navigator.userAgent;
  if (navigator.userAgent.indexOf('MSIE') !=-1) {
    document.write('');
  } else if (navigator.userAgent.indexOf('Firefox') !=-1) {
    document.write('');
  } else if (navigator.userAgent.indexOf('Chrome') !=-1) {
    document.write('');
  } else if (navigator.userAgent.indexOf('Opera') !=-1) {
    document.write('');
    } else if (navigator.userAgent.indexOf('Safari') !=-1) {
    document.write('');
    alert ("Estas a SAFARI, et recomanem que utilitzis un altre navegador");
  } else {
    document.write('');
  }
// ]]></script>
</head>

<body>
<header>
	<div class="page-header">
		<h1>Inscripció Balles</h1>
	</div>
</header>
   <div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Inicia Sessió per Inscriure Jugadors</h1>
            <div class="account-wall">
                <img class="profile-img" src="../images/logoBalles.gif"
                    alt="">
                <form class="form-signin" method="post">
                <input type="text" class="form-control" name="username" id="username" placeholder="usuari" autofocus>
                <input type="password" class="form-control" name="password" id="password" placeholder="contrasenya" >
                <button class="btn btn-lg btn-primary btn-block" type="submit">
                    Sign in</button>
                 <span>Si no tens usuari sol.licita un a <a  href="mailto:laballes@basquetsabadell.com" class="text-center new-account">laballes@basquetsabadell.com</a></span>
                </form>
            </div>
        </div>
    </div>
</div>
<footer id="main-footer">
	
		<p><a target="_blank" href="http://basquetsabadell.com/laballes/"><img style="width:100px; height:100px;" src="../../images/logoBalles.gif"></a><a target="_blank" href="https://twitter.com/la_balles"><img style="width:100px; height:100px;" src="../app/images/twitter.png"></a><a target="_blank"  href="https://www.facebook.com/laballes.basquetlleuresbd"> <img style="width:100px; height:100px;" src="../app/images/facebook.png"></a></p>
	
	</footer>
</body>

</html>