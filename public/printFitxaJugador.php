                                <?php @session_start();
include('../app/config.ini.php');
include('../app/login.lib.php');
$nom= $_POST['nom'];
//echo $nom;
$idJugador= '';
for( $index = 0; $index < strlen($nom); $index++ )
    {
        if( is_numeric($nom[$index]) )
        {
            $idJugador.= $nom[$index];
        }
    }  
//echo $idJugador;
require_once('../app/fpdf/fpdf.php');
define("Alturacarnet",0);
define("HPag",721.89);
define("MargeCarnet_a_Esquerra",100);
$idEquip = $_SESSION['USUARIO']['idEquip'];
$arrDadesJugador = array();
$arrDadesEquip = array();
$arrDadesJugador = getDadesFitxa($idJugador);
//$arrDadesEquip = getDadesEquip($idEquip);

function pintar_carnet($dadesJugador,&$pdf,$contador,$x,$y){
	$pdf->Rect($x, $y, 60, 35 );
	//$pdf->Rect($x+2,$y+2,20,25);
	if ($dadesJugador['foto'] != ''){
		$pdf->Image($dadesJugador['foto'],$x+2,$y+8.5,20,22,'JPG');
	}else{
		$pdf->Image('../app/images/user.jpg',$x+2,$y+8.5,20,25,'JPG');
	}
	$y += 5;
	$pdf->SetFont('Arial','B',9);
	$pdf->Text($x+2,$y-1,"Associacio Basquet Lleure Sabadell");
	$pdf->SetFont('Arial','B',10);
	$nomEquip = $dadesJugador['nomEquip'];
	if(strlen($nomEquip)> 25) {
		$pdf->SetFont('Arial','',6);
	}
	$pdf->Text($x+2,$y+2,"Equip : ".$dadesJugador['nomEquip']);
	$pdf->SetFont('Arial','B',9);
	$y += 5;
	$pdf->Text($x+25,$y,$dadesJugador['nomDivisio']);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	$pdf->Text($x+25,$y,strtoupper($dadesJugador['nomJugador']));
	$y += 3.5;
	$cognoms = strtoupper($dadesJugador['cognomsJugador']);
	if(strlen($cognoms )> 16) {
		$pdf->SetFont('Arial','',7);
	}
	$pdf->Text($x+25,$y,$cognoms);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	$pdf->Text($x+25,$y,$dadesJugador['dni']);
	$y += 4;
	$pdf->Text($x+25,$y,$dadesJugador['dataNeixament']);
	$y += 4;
	$poblacio = strtoupper($dadesJugador['poblacio']);
	if(strlen($poblacio )> 17) {
		$pdf->SetFont('Arial','',6);
	}
	$pdf->Text($x+25,$y,$poblacio);
	$pdf->SetFont('Arial','',9);
	$y += 4;
	
}
$contador = 0;
$y=10;
$x=10;
$h = 0;
$i = 0;
$pdf = new FPDF();
$pdf->AddPage();
$pdf->SetFont('Arial','',10);

if($arrDadesJugador==null) echo 'es null';
	pintar_carnet($arrDadesJugador,$pdf,$contador,$x,$y);
	$contador++;
	//$num--;
//}
$pdf->Output();
?>
                            