<?php @session_start();include('../app/config.ini.php');
if($_SESSION['USUARIO'] == '' ){header('location:index.php');}
if ($_SESSION['USUARIO']['rol'] == 1){
	$idEquip = $_REQUEST['idEquip'];
	$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}
include('../app/login.lib.php');
//print_r($_SESSION['USUARIO']['idEquip']);
//$idEquip = $_SESSION['USUARIO']['idEquip'];
//echo $idEquip;
$equip = getNomEquip();
$nomEquip = $equip['nomEquip'];
$numJugadorsEquip = numJugadorsEquip($idEquip);
if ($numJugadorsEquip == ''){$numJugadorsEquip = '0';}
$arrDadesEquip = getDadesEquip($idEquip);
$arrDadesJugadors = getDadesTriptic($idEquip);
$observacio = getObservacioEquip($idEquip);
?>
<!DOCTYPE html>

<head><META http-equiv=Content-Type content="text/html; charset=UTF-8"> 
<link rel="shortcut icon" type="image/x-icon" href="../app/images/logoBalles.ico" />
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<title>Inscripcio Balles</title>

<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.min.css">
<script src="../app/js/jquery-1.10.2.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.js"></script>
 <script type="text/javascript" src="../app/js/jquery.tablesorter.min.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../app/js/scripts.js"></script>
<link rel="stylesheet" href="../app/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../app/css/estil.css" type="text/css" media="all" />
<link rel="stylesheet" href="../app/bootstrap/css/signin.css">

<script type="text/javascript">/* <![CDATA[ */

		
</script>
</head>

<body>
<header>
<h1>Inscripcio Balles</h1>
<h2><?php print_r($nomEquip); ?></h2>
</header>
<div class="container">
<a style="float:right"; href="../app/logout.php"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>

<div style="margin-top:4%"; class="panel panel-default">
  <div style="text-align:center"; class="panel-heading">PAGAMENT <input style="margin-left:15%;" type="button" class="btn btn-warning" value="Calcular" onclick="open('calculadora.html','calculadora', 'width=340,height=300,toolbar=no, statusbar=no,menubar=no,titlebar=no')" /></div>
  <table class="table">
  <tr>
	<th>EQUIP</th><th>JUGADORS</th><th>TOTAL FITXES</th><th>Pista Propia</th><th>Esportivitat</th><th>Prefència</th><th>INSCRIPCIO</th><th>TOTAL A PAGAR</th><th>Pagat</th><?php if ($_SESSION['USUARIO']['rol'] == 1){ ?><th>Pagament</th> <?php } ?> 
  </tr>
   	<?php getPagament($idEquip) ?>
  </table>
</div>
<?php if ($_SESSION['USUARIO']['rol'] == 1){  
	echo "<a href='printRebut.php' target='_blank'><button type='button' class='btn btn-primary'>Imprimir Rebut</button></a>";
	//echo "<a href='adminBalles.php'><button style='float:right'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
	/* }else{ 
	echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";*/
	 } ?>
<div style="margin-top:4%"; class="panel panel-default">
 
  <div style="text-align:center"; class="panel-heading">Jugadors Inscrits  <span class="red">El teu equip te <?php echo $numJugadorsEquip; ?> jugadors inscrits</span>
 <?php if ($_SESSION['USUARIO']['rol'] == 1){  ?>
  <FORM action="../app/afegirObservacio.php?idEquip=<?php echo $idEquip ?>" method="post">
    <LABEL for="observacio">Observacions </LABEL>
              <INPUT type="text" id="obsEquip" name="obsEquip" value ='<?php echo ($observacio[0]['observacions']); ?>'>    
    <INPUT type="submit" value="Afegir"> 
 
 </FORM>
 <?php } ?>
 </div>
  <table class="table"  id="keywords">
  <tr>
	<th>JUGADOR</th><!--<th>Foto</th>--><th>DNI</th><th>Nom</th><th>Edat</th><th>Email</th><th>Telefon</th><th>Tipus fitxa</th>
  </tr>
  	<?php if($idEquip == 52 && $_SESSION['USUARIO']['rol'] == 1){array_pop($arrDadesJugadors); array_pop($arrDadesJugadors); }?> 
   	<?php mostraJugadors($arrDadesJugadors)?>
  </table>
</div>
	<?php if ($_SESSION['USUARIO']['rol'] == 1){  
		echo "<a href='printTriptic.php' target='_blank'><button type='button' class='btn btn-primary'>Imprimir Triptic</button></a>";
		echo "<a href='adminBalles.php'><button style='float:right'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
	}else{ 
		echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
	 } ?>
</div>
<footer id="main-footer">
	
		<p><a target="_blank" href="http://basquetsabadell.com/laballes/"><img style="width:100px; height:100px;" src="../../images/logoBalles.gif"></a><a target="_blank" href="https://twitter.com/la_balles"><img style="width:100px; height:100px;" src="../app/images/twitter.png"></a><a target="_blank"  href="https://www.facebook.com/laballes.basquetlleuresbd"> <img style="width:100px; height:100px;" src="../app/images/facebook.png"></a></p>
	
	</footer>
	<script type="text/javascript">
$(function(){
  $('#keywords').tablesorter(); 
});
</script>
</body>
</html>