<?php @session_start();include('../app/config.ini.php');
include('../app/login.lib.php');
//print_r($_SESSION['USUARIO']['idEquip']);
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php');}
$idEquip = $_SESSION['USUARIO']['idEquip'];
//echo $idEquip;
$equip = getNomEquip();
$nomEquip = $equip['nomEquip'];
$numJugadorsEquip = numJugadorsEquip($idEquip);
$arrDadesEquip = getDadesEquip($idEquip);
$arrDadesJugadors = getDadesTriptic($idEquip);
$numEquips = numEquipsInscrits();
$numJugadorsTotals = numJugadorsTotals();
$pass = crearPass();
?>
<!DOCTYPE html>

<head>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
  d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
  _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
  $.src="//v2.zopim.com/?3fARb25zx2ECLD3pD7VaxbIR3sKEKAB2";z.t=+new Date;$.
  type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<title>Inscripcio Balles</title>
<META http-equiv=Content-Type content="text/html; charset=ISO-8859-1"> 
<link rel="shortcut icon" type="image/x-icon" href="../app/images/logoBalles.ico" />
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.min.css">
<script src="../app/js/jquery-1.10.2.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../app/js/scripts.js"></script>
<link rel="stylesheet" href="../app/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../app/css/estil.css" type="text/css" media="all" />
<link rel="stylesheet" href="../app/bootstrap/css/signin.css">
	<?php
		$con = "select equip.nomEquip from equip where idEquip <> 1 and actiu='ACT'";
		$query = mysql_query($con);
	?>
    <script>
		$(function() {
		<?php
			while($row= mysql_fetch_array($query)) {//se reciben los valores y se almacenan en un arreglo
	      		$elementos[]= '"'.$row['nomEquip'].'"';
		 	}
			$arreglo= implode(", ", $elementos);//junta los valores del array en una sola cadena de texto
		?>	
			var availableTags=new Array(<?php echo $arreglo; ?>);//imprime el arreglo dentro de un array de javascript
			$( "#tags" ).autocomplete({
				source: availableTags
			});
		});
	</script>
	<script>
	ga(‘set’, ‘&uid’, {{USER_ID}}); // Establezca el ID de usuario mediante el user_id con el que haya iniciado sesión.
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58844763-2', 'auto');
  ga('send', 'pageview');
  

</script>
</head>

<body>
<header>
<h1>Inscripcions Balles</h1>
<h2><?php print_r($nomEquip); ?></h2>
</header>
<div class="container">
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=1&&nomDivisio=Primera Masculina"><button type="button" class="btn btn-success" id="delete">Primera Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=2&&nomDivisio=Segona Masculina"><button type="button" class="btn btn-success" id="delete">Segona Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=5&&nomDivisio=Tercera Masculina"><button type="button" class="btn btn-success" id="delete">Tercera Masculina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=3&&nomDivisio=Primera Femenina"><button type="button" class="btn btn-success" id="delete">Primera Femenina</button></a>
<a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=4&&nomDivisio=Segona Femenina"><button type="button" class="btn btn-success" id="delete">Segona Femenina</button></a>
<a style="margin-left:1%"; href="../app/exportJugadorsExcel.php"><button type="button" class="btn btn-warning" id="delete">Exportar Jugadors</button></a>

<a style="float:right"; href="../app/logout.php"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<?php if($_SESSION['USUARIO']['nomUser'] == 'david'){ ?><a style="float:right"; href="../app/sessions.txt" target="_blank"><button type="button" class="btn btn-info" id="delete">Sessions</button></a> <?php } ?>
<div style="padding: 2% 2%;" >
   <form class="bs-example bs-example-form" role="form" action="../app/getIdEquip.php?$_GET['nomDesti'];" method="GET">
      <div class="row">
         <div class="col-lg-6">
            <div class="input-group">
               <input type="text" class="form-control" id="tags" name="nomEquip" placeholder="Introdueix un equip...">
               <span class="input-group-btn">
                  <button style="margin-left: 5%;" class="btn btn-warning" type="submit">Buscar Equip</button>
                   <a style="margin-left:2%"; href="rebutManual.php?"><button type="button" class="btn btn-primary" id="delete">Rebut Manual</button></a>
                   <a style="margin-left:2%"; href="printFitxa.php?"><button type="button" class="btn btn-primary" id="delete">Fitxa Jugador</button></a> 
              </span>
            </div><!-- /input-group -->
         </div><!-- /.col-lg-6 -->
      </div><!-- /.row -->
   </form>
</div>	
<div style="margin-top:4%"; class="panel panel-danger">
 
  <div style="text-align:center"; class="panel-heading">Equips Inscrits<span class="red"> <?php echo $numEquips-1; ?></span> / Jugadors Totals : <span class="red"><?php echo $numJugadorsTotals; ?></span></div>
  <table class="table">
  <tr>
		<th>ID EQUIP</th><th>Nom Equip</th><th>Jugadors</th><th>Usuari</th><th>Contrasenya</th><th>Divisio</th><th>Observacions</th><th>Pagat</th><th>Pagament</th>
	</tr>
   	<?php mostraEquips()?>
  </table>
</div>
	
<FORM action="../app/crearEquip.php" method="post">
    <P>
    <LABEL for="nom">Nom Equip: </LABEL>
              <INPUT type="text" id="nomEquip" name="nomEquip"><BR>
    <LABEL for="usuari">Nom Usuari: </LABEL>
              <INPUT type="text" id="usuari" name="usuari"><BR>
   <LABEL for="pass">Contrasenya: </LABEL>
              <INPUT type="text" readonly="yes" id="pass" name="pass" value="<?php echo $pass; ?>"><BR>
    <INPUT type="submit" value="Crear Equip"> <INPUT type="reset">
    </P>
 </FORM>
 </div>
 <footer id="main-footer">
	
		<p><a target="_blank" href="http://basquetsabadell.com/laballes/"><img style="width:100px; height:100px;" src="../../images/logoBalles.gif"></a><a target="_blank" href="https://twitter.com/la_balles"><img style="width:100px; height:100px;" src="../app/images/twitter.png"></a><a target="_blank"  href="https://www.facebook.com/laballes.basquetlleuresbd"> <img style="width:100px; height:100px;" src="../app/images/facebook.png"></a></p>
	
	</footer>
</body>
</html>