<?php @session_start();include('../app/config.ini.php');
/*if ($_SESSION['USUARIO']['rol'] == 1){
	$idEquip = $_REQUEST['idEquip'];
	$_SESSION['USUARIO']['idEquip'] = $idEquip;	
	//echo "es rol 1";
}else{
	$idEquip = $_SESSION['USUARIO']['idEquip'];
}*/
if($_SESSION['USUARIO'] == '' || $_SESSION['USUARIO']['rol'] != 1){header('location:index.php');}
include('../app/login.lib.php');
$idDivisio = $_REQUEST['idDivisio'];
if ($idDivisio == ''){header('location:index.php');}
$divisio = getNomDivisio($idDivisio);
$nomDivisio = $_REQUEST['nomDivisio'];
//$nomEquip = $equip['nomEquip'];
//$arrDadesEquip = getEquipsByidDivisio($idDivisio);
?>
<!DOCTYPE html>

<head><META http-equiv=Content-Type content="text/html; charset=ISO-8859-1"> 
<link rel="shortcut icon" type="image/x-icon" href="../app/images/logoBalles.ico" />
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<title>Inscripcio Balles</title>

<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.min.css">
<script src="../app/js/jquery-1.10.2.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../app/js/scripts.js"></script>
<link rel="stylesheet" href="../app/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../app/css/estil.css" type="text/css" media="all" />
<link rel="stylesheet" href="../app/bootstrap/css/signin.css">

</head>

<body>
<header>
<h1>Inscripcio Balles</h1>
<h2><?php echo ($nomDivisio); ?></h2>
</header>
<div class="container">
<?php if ($_SESSION['USUARIO']['rol'] == 1){  
	echo "<a href='adminBalles.php'><button style='margin-left:2%'; type='button' class='btn btn-warning'>Tornar a Administracio</button></a>";
	 }else{ 
	echo "<a href='formInscripcio.php'><button style='float:right'; type='button' class='btn btn-warning'>Inscrire mes jugadors</button></a>";
	 } ?>
<?php if($nomDivisio != 'Primera Masculina'){ ?><a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=1&&nomDivisio=Primera Masculina"><button type="button" class="btn btn-success" id="delete">Primera Masculina</button></a> <?php } ?>
<?php if($nomDivisio != 'Segona Masculina'){ ?><a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=2&&nomDivisio=Segona Masculina"><button type="button" class="btn btn-success" id="delete">Segona Masculina</button></a><?php } ?>
<?php if($nomDivisio != 'Tercera Masculina'){ ?><a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=5&&nomDivisio=Tercera Masculina"><button type="button" class="btn btn-success" id="delete">Tercera Masculina</button></a><?php } ?>
<?php if($nomDivisio != 'Primera Femenina'){ ?><a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=3&&nomDivisio=Primera Femenina"><button type="button" class="btn btn-success" id="delete">Primera Femenina</button></a><?php } ?>
<?php if($nomDivisio != 'Segona Femenina'){ ?><a style="margin-left:1%"; href="equipsPerDivisio.php?idDivisio=4&&nomDivisio=Segona Femenina"><button type="button" class="btn btn-success" id="delete">Segona Femenina</button></a><?php } ?></br>
<?php echo "<a style='margin-left:2%'; href='../app/exportDadesEquipExcel.php?idDivisio=$idDivisio'><button type='button' class='btn btn-primary' id='delete'>Exportar Dades Equips</button></a>";?>
<a style="float:right"; href="../app/logout.php"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<div style="margin-top:4%"; class="panel panel-success">
  <div style="text-align:center;font-size:22px;" class="panel-heading"><b>EQUIPS PER DIVISIO</b></span>
 </div>
  <table class="table">
 
  <?php if ($nomDivisio == 'Primera Masculina' || $nomDivisio == 'Segona Masculina') { ?>
   	<tr>
		<th>idEquip</th><th>Equip</th><th>capita</th><th>Email</th><th>Telefon</th><th>Preferencia Partits</th><th>Observacions</th>
  	</tr>
  	<?php getEquipsByidDivisio($idDivisio)?>
  <?php }else{ ?>
 	 <tr>
  		<th>idEquip</th><th>Equip</th><th>capita</th><th>Email</th><th>Telefon</th><th>Preferencia Partits</th><th>Direccio Pista</th><th>Horari Partit</th><th>Poblacio</th><th>Observacions</th>
  	</tr>
   	<?php getEquipsByidDivisioPista($idDivisio)?>
   	<?php } ?>
  </table>
</div>

</div>
<footer id="main-footer">
	
		<p><a target="_blank" href="http://basquetsabadell.com/laballes/"><img style="width:100px; height:100px;" src="../../images/logoBalles.gif"></a><a target="_blank" href="https://twitter.com/la_balles"><img style="width:100px; height:100px;" src="../app/images/twitter.png"></a><a target="_blank"  href="https://www.facebook.com/laballes.basquetlleuresbd"> <img style="width:100px; height:100px;" src="../app/images/facebook.png"></a></p>
	
	</footer>
</body>
</html>