<?php @session_start();
if ($_SESSION['USUARIO']['rol'] == 1){
	header('location:adminBalles.php');	
	//echo "es rol 1";
}
include('../app/config.ini.php');
include('../app/login.lib.php');
//print_r($_SESSION['USUARIO']['idEquip']);
$idEquip = $_SESSION['USUARIO']['idEquip'];
//echo $idEquip;
$equip = getNomEquip();
$nomEquip = $equip['nomEquip'];
$numJugadorsEquip = numJugadorsEquip($idEquip);
$arrDadesEquip = getDadesEquip($idEquip);
?>
<!DOCTYPE html>

<head>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
	d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
	_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
	$.src="//v2.zopim.com/?3fARb25zx2ECLD3pD7VaxbIR3sKEKAB2";z.t=+new Date;$.
	type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zopim Live Chat Script-->
<meta http-equiv="Content-Type" content="text/html; charset=big5">
<title>Inscripcio Balles</title>
 <link rel="shortcut icon" type="image/x-icon" href="../app/images/logoBalles.ico" />
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">
<meta content="" name="description">
<meta content="" name="author">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.css">
<link rel="stylesheet" href="../app/css/dark-hive/jquery-ui-1.10.4.custom.min.css">
<script src="../app/js/jquery-1.10.2.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.js"></script>
<script src="../app/js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="../app/js/scripts.js"></script>
<link rel="stylesheet" href="../app/css/estil.css" type="text/css" media="all" />
<link rel="stylesheet" href="../app/bootstrap/css/bootstrap.min.css">


<link rel="stylesheet" href="../app/bootstrap/css/signin.css">
<script type="text/javascript">
alert("Els pagaments en efectiu a les oficines, nomes podran ser dimarts, dijous i divendres");
$(document).ready(function() {
	$("#dadesPista").click(function(){
		$("#pista").slideToggle("slow");
	});
	$("#preferenciaHoraris").click(function(){
		$("#preferencia").slideToggle("slow");
	});
	$("#afegir").click(function(){
		$("#buttons").slideToggle("slow");
	});
});
function mostrarOcultarPista(obj) {
	pista.style.visibility = (obj.checked) ? 'visible' : 'hidden';
}
function mostrarOcultarPreferencia(obj) {
	preferencia.style.visibility = (obj.checked) ? 'visible' : 'hidden';
}
</script>
</head>

<body>
<header>
<h1>Inscripcio Balles</h1>
<h2><?php print_r($nomEquip); ?></h2>
</header>
<div class="container">
<a style="float:right"; href="../app/logout.php"><button type="button" class="btn btn-danger" id="delete">Tancar Sessio</button></a>
<fieldset>
<span class="red">* TOTS ELS CAMPS DELS JUGADORS MENYS EL DORSAL SON OBLIGATORIS * La inscripcio no sera valida si no estan totes les dades, foto i dni</span></br>
<div id="num">
<?php numJugadorsEquip($idEquip); ?>
	Integrants de l'equip ja inscrits :<input type='text' id='jugadorsInscrits' name='jugadorsInscrits' readonly='yes' size='2' value='<?php  echo $numJugadorsEquip;?>'></div>
<div id="opcio">Escull el numero jugadors :
	<select id="columnes" name="columnes">
		<option name="1" value=1>1 jugador</option>
		<option name="2" value=2>2 jugadors</option>
		<option name="3" value=3>3 jugadors</option>
		<option name="4" value=4>4 jugadors</option>
		<option name="5" value=5>5 jugadors</option>
		<option name="6" value=6>6 jugadors</option>
		<option name="7" value=7>7 jugadors</option>
		<option name="8" value=8>8 jugadors</option>
		<option name="9" value=9>9 jugadors</option>
		<option name="10" value=10>10 jugadors</option>
		<!--<option name="11" value=11>11 jugadors</option>
		<option name="12" value=12>12 jugadors</option> -->
	</select>
	
</div>

<div id="buttons">
<button id="afegir">Afegir</button>
<!--<button id="delete">Eliminar tot</button>
<!--<button id="codi">Mostra codi</button>-->
</div>
</fieldset>
<div class="limpio"></div>
<div class="register">
<?php echo "<form class='form-horizontal' action='ok.php?idEquip=$idEquip' method='post' enctype='multipart/form-data' id='registre'>"; ?>
<fieldset>
<label for='Divisio '>Divisio:</label>
<select id="divisio" name="divisio" >
<option name="1" value=1>Sense determinar</option>
<option name="2" <?php if ($arrDadesEquip['nomDivisio'] == 'Primera Masculina'){ echo "selected"; } ?> value=2>Primera Divisio Masculina</option>
<option name="3" <?php if ($arrDadesEquip['nomDivisio'] == 'Segona Masculina'){ echo "selected"; } ?> value=3>Segona Divisio Masculina</option>
<option name="4" <?php if ($arrDadesEquip['nomDivisio'] == 'Primera Femenina'){ echo "selected"; } ?> value=4>Primera Divisio Femenina</option>
<option name="5" <?php if ($arrDadesEquip['nomDivisio'] == 'Segona Femenina'){ echo "selected"; } ?> value=5>Segona Divisio Femenina</option>
<option name="6" <?php if ($arrDadesEquip['nomDivisio'] == 'Tercera Masculina'){ echo "selected"; } ?> value=6>Tercera Divisio Masculina</option>
</select>
		<label for='Samarreta'>Color Samarreta:</label><input  title='Escull un color' type='text' required='yes' name='colorSamarreta' id='colorSamarreta' value='<?php echo $arrDadesEquip['colorSamarreta']; ?>' >
		<label for='Pantalo'>Color Pantalo:</label><input title='Escull un color' type='text' required='yes' name='colorPantalo' id='colorPantalo' value='<?php echo $arrDadesEquip['colorPantalo']; ?>' >
		</fieldset>
		<input type="checkbox" name="preferenciaHoraris" value="1" id="preferenciaHoraris"> Afegir preferencia horaris</br>
		<div style="display:none" id="preferencia">
			Preferencia<input type="text"size=40 id="preferenciaHorari" name="preferenciaHorari" value='<?php echo $arrDadesEquip['preferenciaPartit']; ?>'>
		</div></br>
		<input type="checkbox" name="dadesPista" value="1" id="dadesPista"> <!--onClick="mostrarOcultarPista(this)"-->Afegir pista (nomes si ets equip Femeni amb Pista Propia)
		<div style="display:none" id="pista">
			Nom de la Pista:<input type="text" name="nomPista" id="nomPista" value='<?php echo $arrDadesEquip['nomPista']; ?>'>
			horari Partit:<input type="text" name="horaPista" id="horaPista" value='<?php echo $arrDadesEquip['horariPista']; ?>'>
			Poblacio:<input type="text" name="poblacioPista" id="poblacioPista" value='<?php echo $arrDadesEquip['poblacioPista']; ?>'></br>
			Direccio:<input type="text" name="direccioPista" id="direccioPista" size=100 value='<?php echo $arrDadesEquip['direccioPista']; ?>'>
		</div>

		<div id="pagina"></div>
		<input class="btn btn-success" type="submit" value="Registrar">
	</form>
	
	</div>
	<!--<div id="mostraCodi" title="Codi Layout">
	<p></p>
		</div>-->
</div>
<footer id="main-footer">
	
		<p><a target="_blank" href="http://basquetsabadell.com/laballes/"><img style="width:100px; height:100px;" src="../../images/logoBalles.gif"></a><a target="_blank" href="https://twitter.com/la_balles"><img style="width:100px; height:100px;" src="../app/images/twitter.png"></a><a target="_blank"  href="https://www.facebook.com/laballes.basquetlleuresbd"> <img style="width:100px; height:100px;" src="../app/images/facebook.png"></a></p>
	
	</footer>
</body>
</html>