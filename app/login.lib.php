                                <?php
/**
 * valida un usuario y contraseña
 * @param string $usuario
 * @param string $password
 * @return bool
 */
function conecta() {
	$link = mysql_connect ( SERVIDOR_MYSQL, USUARIO_MYSQL, PASSWORD_MYSQL );
	
	if (!$link) {
		trigger_error ( 'Error al conectar al servidor mysql: ' . mysql_error (), E_USER_ERROR );
	}
	// Seleccionar la base de datos activa
	$db_selected = mysql_select_db ( BASE_DATOS, $link );
	if (!$db_selected) {
		trigger_error ( 'Error al conectar a la base de datos: ' . mysql_error (), E_USER_ERROR );
	}
}
function login($nickName, $password) {
	conecta ();
	// 1 - preparamos la consulta SQL a ejecutar utilizando sólo el usuario y evitando ataques de inyección SQL.
	$query = 'SELECT ' . CAMPO_USUARIO_LOGIN . ', ' . CAMPO_CLAVE_LOGIN . ' FROM ' . TABLA_DATOS_LOGIN . ' WHERE ' . CAMPO_USUARIO_LOGIN . '="' . mysql_real_escape_string ( $nickName ) . '" and actiu="ACT" LIMIT 1 '; // la tabla y el campo se definen en los parametros globales
	$queryUser = 'SELECT * FROM ' . TABLA_DATOS_LOGIN . ' WHERE ' . CAMPO_USUARIO_LOGIN . '="' . mysql_real_escape_string ( $nickName ) . '" LIMIT 1 ';
	$result = mysql_query ( $query );
	$resultUser = mysql_query ( $queryUser );
	if (! $result) {
		trigger_error ( 'Error al ejecutar la consulta SQL: ' . mysql_error (), E_USER_ERROR );
	}
	// 2 - extraemos el registro de este usuario
	$row = mysql_fetch_assoc ( $result );
	$rowUser = mysql_fetch_assoc ( $resultUser );
	
	if ($row) {
		//return true;
		//print_r($row);
		// 3 - Generamos el hash de la contraseña encriptada para comparar o lo dejamos como texto plano
		switch (METODO_ENCRIPTACION_CLAVE) {
			case 'sha1' | 'SHA1' :
				$hash = sha1 ( $password );
				break;
			case 'md5' | 'MD5' :
				$hash = md5 ( $password );
				break;
			case 'texto' | 'TEXTO' :
				$hash = $password;
				break;
			default :
				trigger_error ( 'El valor de la constante METODO_ENCRIPTACION_CLAVE no es válido. Utiliza MD5 o SHA1 o TEXTO', E_USER_ERROR );
		}
		
		// 4 - comprobamos la contraseña
		if ($hash == $row [CAMPO_CLAVE_LOGIN]) {
			@session_start ();
			$_SESSION ['USUARIO'] = array (
					'nomUser' => $rowUser [CAMPO_USUARIO_LOGIN],
					'idEquip' => $rowUser ['idEquip'],
					'rol' => $rowUser['rol'],
			);
			
			// en este punto puede ser interesante guardar más datos en memoria para su posterior uso, como por ejemplo un array asociativo con el id, nombre, email, preferencias, ....
			return true; // usuario y contraseña validadas
		} else {
			@session_start ();
			unset ( $_SESSION ['USUARIO'] ); // destruimos la session activa al fallar el login por si existia
			return false; // no coincide la contraseña
		}
	} else {
		print_r("usuario no existe".$row);
		// El usuario no existe
		echo "<div style='display:block;'id='MensajeError' class='mensaje error'name='login incorrecte' text='Usuari o contrasenya incorrectes'><a href='../public/index.php'><input type='button'value='usuari o contrasenya incorrectes' class='boton_cerrar' ></a></div>";
		return false;
	}
}
/**
 * Veridica si el usuario está logeado
 *
 * @return bool
 */
function estoy_logeado() {
	@session_start (); // inicia sesion (la @ evita los mensajes de error si la session ya está iniciada)
	
	if (! isset ( $_SESSION ['USUARIO'] ))
		return false; // no existe la variable $_SESSION['USUARIO']. No logeado.
	if (! is_array ( $_SESSION ['USUARIO'] ))
		return false; // la variable no es un array $_SESSION['USUARIO']. No logeado.
	if (empty ( $_SESSION ['USUARIO'] ['user'] ))
		return false; // no tiene almacenado el usuario en $_SESSION['USUARIO']. No logeado.
			              // cumple las condiciones anteriores, entonces es un usuario validado
	return true;
}
/**
 * Vacia la sesion con los datos del usuario validado
 */
function logout() {
	@session_start (); // inicia sesion (la @ evita los mensajes de error si la session ya está iniciada)
	unset ( $_SESSION ['USUARIO'] ); // eliminamos la variable con los datos de usuario;
	session_write_close (); // nos aseguramos que se guarda y cierra la sesion
	return true;
}
function getRealIP() {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
       
        return $_SERVER['REMOTE_ADDR'];
    }
    
function delCaractersEspecials($string)
{

    //$string = trim($string);

    $string = str_replace(
        array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
        array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
        $string
    );

    $string = str_replace(
        array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
        array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
        $string
    );

    $string = str_replace(
        array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
        array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
        $string
    );

    $string = str_replace(
        array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
        array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
        $string
    );

    $string = str_replace(
        array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
        array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
        $string
    );

    $string = str_replace(
        array('ñ', 'Ñ', 'ç', 'Ç'),
        array('n', 'N', 'c', 'C',),
        $string
    );

    //Esta parte se encarga de eliminar cualquier caracter extraño
    $string = str_replace(
        array("\\", "¨", "º", "-", "~",
             "#", "@", "|", "!", "\"",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "`", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             "."),
        '',
        $string
    );


    return $string;
}

function getAgeByBirthDay($birthDay){ 
 	$day_diff   = date("d") - date("d", strtotime($birthDay));   
 	$month_diff = date("m") - date("m", strtotime($birthDay));     
        $year_diff  = date("Y") - date("Y", strtotime($birthDay)); 
        if ($month_diff < 0) $year_diff--;    
        elseif (($month_diff==0) && ($day_diff < 0))$year_diff--;
        return $year_diff;	
}
	
function calcularEdad ($fecha_nacimiento) {
   $aFecha = explode( '/', $fecha_nacimiento);
	$edad = floor(( (date("Y") - $aFecha[2] ) * 372 + ( date("m") - $aFecha[1] ) * 31 + Date("d" ) - $aFecha[0] )/372) ;
	return $edad;
}


function getNomEquip() {
	$i = 0;
	conecta ();
	$user = $_SESSION ['USUARIO'] ['nomUser'];
	$idEquip = $_SESSION['USUARIO']['idEquip'];
	$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
	$queryEquip = mysql_query ( 'SELECT nomEquip from equip
					inner join usuari on usuari.idEquip = equip.idEquip
					WHERE usuari.idUser = "' . $idUser [0] . '" and equip.actiu = "ACT" LIMIT 1'  );
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row;
}
function getNomDivisio($idDivisio) {
	$i = 0;
	conecta ();
	$queryEquip =  mysql_query  ( 'select divisio.nomDivisio where divisio.idDivisio="' . ($idDivisio) . '" '  );
	return $queryEquip;
}

function numJugadorsEquip($idEquip){
	conecta();
	$sql = "SELECT count(jugador.idJugador) FROM jugador WHERE jugador.idEquip = '".$idEquip."' and jugador.actiu = 'ACT' ";
	$result = mysql_query ( $sql );
	$row = mysql_fetch_row ( $result );
	//print_r($row);
	//echo "Integrants de l'equip ja inscrits :<input type='text' readonly='yes' size='2' value='".$row[0]."'>";
	return $row[0];
}
function numEquipsInscrits(){
	conecta();
	$sql = "SELECT count(equip.idEquip) FROM equip where actiu = 'ACT' ";
	$result = mysql_query ( $sql );
	$row = mysql_fetch_row ( $result );
	//print_r($row);
	//echo "Integrants de l'equip ja inscrits :<input type='text' readonly='yes' size='2' value='".$row[0]."'>";
	return $row[0];
}

function numJugadorsTotals(){
	conecta();
	$sql = "SELECT count(jugador.idJugador) FROM jugador where actiu = 'ACT' ";
	$result = mysql_query ( $sql );
	$row = mysql_fetch_row ( $result );
	//print_r($row);
	//echo "Integrants de l'equip ja inscrits :<input type='text' readonly='yes' size='2' value='".$row[0]."'>";
	return $row[0];
}

function insertJugador($idEquip,$dni,$nom,$cognoms,$email,$dorsal,$dataNeixament,$poblacio,$telefon,$isPrimerCapita,$isSegonCapita,$isEntrenador,$foto,$dniAdj){
	conecta ();
	$nom = delCaractersEspecials($nom);
	$cognoms = delCaractersEspecials($cognoms);
		$sql = "INSERT INTO jugador(nomJugador,cognomsJugador,dni,email,telefon,dataNeixament,poblacio,dorsal,isPrimerCapita,isSegonCapita,isEntrenador,foto,dniAdj,idEquip,actiu) VALUES ('" . $nom . "', '" . $cognoms . "', '" . $dni . "', '" . $email . "', '" . $telefon . "','" . $dataNeixament . "','" . $poblacio . "','" . $dorsal . "', '" . $isPrimerCapita . "', '" . $isSegonCapita . "','" . $isEntrenador. "', '" . $foto . "','" . $dniAdj . "','" . $idEquip ."','ACT')";
		mysql_query ( $sql );
		//echo"Entra Correctament a INSERT ".$dni."</br>";
		//echo "<script language='JavaScript'>alert('Dni no correcte');</script>";
}

function setDivisio($divisio,$idEquip){
	conecta();
	$sql = "UPDATE equip SET idDivisio='".($divisio-1)."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
	//echo"entra Correctament en Divisio</br>";
}

function setColorSamarreta($colorSamarreta,$idEquip){
	conecta();
	$sql = "UPDATE equip SET colorSamarreta='".$colorSamarreta."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
	//echo"entra Correctament en Samarreta</br>";
}

function setColorPantalo($colorPantalo,$idEquip){
	conecta();
	$sql = "UPDATE equip SET colorPantalo='".$colorPantalo."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
	//echo"entra Correctament en Pantalo</br>";
}

function setPreferenciaHorari($preferencia,$idEquip){
	conecta();
	$sql = "UPDATE equip SET preferenciaPartit='".$preferencia."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
	//echo"entra Correctament en Horari</br>";
}

function equipAmbPistaJoc($idEquip){
	$ret = true;
	conecta();
	$sql = "SELECT * FROM pistaJoc inner join equip on equip.idEquip = pistaJoc.idEquip WHERE pistaJoc.idEquip = '".$idEquip."' and equip.actiu = 'ACT' ";
	$result = mysql_query ( $sql );
	if ($result){
		$row = mysql_fetch_assoc ( $result );
		if ($row){
			$ret = true;
		}else{
			$ret = false;
		}
	}else{
		$ret = false;
	}
	//echo "numero de pistas".$ret;
	return $ret;
}

function setNomPista($nomPista,$idEquip){
	conecta();
	if (equipAmbPistaJoc($idEquip)){
		$sqlPista = "UPDATE pistaJoc SET nomPista='".$nomPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
		mysql_query ( $sqlPista );
		//echo"entra Correctament en Update nomPista</br>";
		//echo $sqlPista;
	}else{
		$sql = "INSERT INTO pistaJoc(nomPista,idEquip) VALUES ('".$nomPista."','".$idEquip."') ";
		mysql_query ( $sql );
		//echo"entra Correctament en Insert nomPista</br>";
	}
	
	//echo"entra Correctament en nomPista</br>";
}

function setHoraPista($horaPista,$idEquip){
	conecta();
	if (equipAmbPistaJoc($idEquip)){
		$sqlPista = "UPDATE pistaJoc SET horariPista='".$horaPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
		mysql_query ( $sqlPista );
		//echo"entra Correctament en Update horaPista</br>";
		//echo $sqlPista;
	}else{
		$sql = "INSERT INTO pistaJoc(horariPista,idEquip) VALUES ('".$horaPista."','".$idEquip."') ";
		mysql_query ( $sql );
		//echo"entra Correctament en Insert horaPista</br>";
	}

	//echo"entra Correctament en nomPista</br>";
}

function setPoblacioPista($poblacioPista,$idEquip){
	conecta();
	if (equipAmbPistaJoc($idEquip)){
		$sqlPista = "UPDATE pistaJoc SET poblacioPista='".$poblacioPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
		mysql_query ( $sqlPista );
		//echo"entra Correctament en Update poblacioPista</br>";
		//echo $sqlPista;
	}else{
		$sql = "INSERT INTO pistaJoc(poblacioPista,idEquip) VALUES ('".$poblacioPista."','".$idEquip."') ";
		mysql_query ( $sql );
		//echo"entra Correctament en Insert poblacioPista</br>";
	}

	//echo"entra Correctament en nomPista</br>";
}

function setDireccioPista($direccioPista,$idEquip){
	conecta();
	if (equipAmbPistaJoc($idEquip)){
		$sqlPista = "UPDATE pistaJoc SET direccioPista='".$direccioPista."' WHERE pistaJoc.idEquip = '".$idEquip."' ";
		mysql_query ( $sqlPista );
		//echo"entra Correctament en Update direccioPista</br>";
		//echo $sqlPista;
	}else{
		$sql = "INSERT INTO pistaJoc(direccioPista,idEquip) VALUES ('".$direccioPista."','".$idEquip."') ";
		mysql_query ( $sql );
		//echo"entra Correctament en Insert direccioPista</br>";
	}

	//echo"entra Correctament en nomPista</br>";
}

function getDadesEquip($idEquip){
	conecta();
	$dades = array();
	$queryEquip = mysql_query('SELECT equip.idEquip, equip.nomEquip, equip.preferenciaPartit, equip.colorPantalo, equip.colorSamarreta, divisio.nomDivisio, 
							pistaJoc.nomPista,pistaJoc.direccioPista,pistaJoc.poblacioPista,pistaJoc.horariPista
			from equip
			LEFT JOIN divisio on equip.idDivisio = divisio.idDivisio
			LEFT JOIN pistaJoc on equip.idEquip = pistaJoc.idEquip
			WHERE equip.idEquip = "' . $idEquip . '" and equip.actiu = "ACT" '  );
	/*while ($desti = mysql_fetch_assoc($queryEquip)){
		array_push($dades, $desti);
	}*/
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row;
}
function getDadesTriptic($idEquip){
	conecta();
	$dades = array();
	//$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
	$queryEquip = mysql_query ( 'SELECT * from jugador
					WHERE jugador.idEquip = "' . $idEquip . '" and jugador.actiu = "ACT" ORDER BY isPrimerCapita DESC , isSegonCapita DESC '  );
	while ($desti = mysql_fetch_assoc($queryEquip)){
	    		array_push($dades, $desti);
	    	}
	return $dades;
}

function mostraJugadors($arrJugadors){
/*foreach ($arrJugadors as $key => $row) {
    $aux[$key] = $row['dataNeixament'];
}
array_multisort($aux, SORT_ASC, $arrJugadors);*/
	$i = 1;
	$cap1 = '';$cap2 = '';$email = ''; $tel = '';$fitxaTipus='';
	foreach ($arrJugadors as $jugador){
	if($jugador['isEntrenador'] == 1){ $fitxaTipus = 'Entrenador'; }else{ $fitxaTipus = 'Jugador'; }
	if($i == 1){$cap1 = '-Primer Capita';$email =$jugador['email'];$tel = $jugador['telefon'];}else{$cap1 = '';$email = '-'; $tel = '-';}
	if($i == 2){ $cap2 = '-Segon Capita';$email =$jugador['email'];$tel = $jugador['telefon'];}else{$cap2 = '';}
		echo "<tr><td>jugador ".$i.$cap1.$cap2."</td><td>".$jugador['dni']."</td><td>".$jugador['nomJugador']." ".$jugador['cognomsJugador']."</td><td>".calcularEdad($jugador['dataNeixament'])."</td><td>".$email."</td><td>".$tel."</td><td>".$fitxaTipus."</td></tr>";
		$i++;
		//<td><img style='width:50px; height:50px;' src='".$jugador['foto']."'></td>
	}
}

function mostraEquips(){
	conecta();
	$dades = array();
	//$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
	$queryEquip = mysql_query ( 'SELECT equip.idEquip,equip.nomEquip,usuari.nomUser, usuari.pass, equip.observacions, divisio.nomDivisio, equip.isPagat from equip 
	inner join usuari on usuari.idEquip = equip.IdEquip 
	left join divisio on divisio.idDivisio = equip.idDivisio where equip.actiu = "ACT" order by equip.nomEquip ASC');
	while ($desti = mysql_fetch_assoc($queryEquip)){
		$idEquip = $desti['idEquip'];
		$pagament = $desti['isPagat'];
		if ($pagament == 0){
			$isPagat = 'NO';
			$pagament = "<a href='../app/afegirPagament.php?idEquip=$idEquip&&isPagat=1'><button type='button' class='btn btn-success'>PAGAR</button></a>";
		}else{
			$isPagat = 'SI';
			$pagament = "<a href='../app/afegirPagament.php?idEquip=$idEquip&&isPagat=0'><button type='button' class='btn btn-danger'>TREURE PAGAMENT</button></a>";
		}
		if ($desti['idEquip'] != 1){
                $numJugadorsEquip = numJugadorsEquip($desti['idEquip']);
                if ($desti['idEquip'] == 52){$numJugadorsEquip -= 2;}
		if ($desti['nomDivisio'] == ''){$nomDivisio = 'Sense Determinar';}else{$nomDivisio = $desti['nomDivisio'];}
	    	echo "<tr><td>".$desti['idEquip']."</td><td><a href='mostraDadesEquip.php?idEquip=".$desti['idEquip']."'>".$desti['nomEquip']."</a></td><td><b>".$numJugadorsEquip."</b></td><td>".$desti['nomUser']."</td><td>".$desti['pass']."</td><td>".$nomDivisio ."</td><td>".$desti['observacions']."</td><td>".$isPagat."</td><td>".$pagament."</td></tr>";
	    	}
	}
	return $dades;
}

function crearPass(){
	$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
	$cad = "";
	for($i=0;$i<7;$i++) {
		$cad .= substr($str,rand(0,62),1);
	}
	return $cad;
}
	
function crearEquip($nomEquip,$usuari,$pass){
	conecta();
	$sql = "INSERT INTO equip(nomEquip,actiu) VALUES ('" . $nomEquip. "','ACT')";
	mysql_query ( $sql );
	$idEquip = mysql_insert_id();
	$sqlUser = "INSERT INTO usuari(nomUser,pass,idEquip,actiu) VALUES ('" . $usuari. "','" . $pass. "','" . $idEquip. "', 'ACT') ";
	mysql_query ( $sqlUser );
	/*mkdir("../images/".$idEquip."/", 0777);
	mkdir("../images/".$idEquip."/foto/", 0777);
	mkdir("../images/".$idEquip."/dni/", 0777);*/
	mkdir('images/'.$idEquip,0777);
	$ruta = ('images/'.$idEquip);
	chmod($ruta,0777);
	mkdir($ruta.'/foto');
	chmod($ruta.'/foto',0777);
	mkdir($ruta.'/dni');
	chmod($ruta.'/dni',0777);
}

function afegirObservacio($idEquip,$observacio){
	conecta();
	$sql = "UPDATE equip SET observacions='".$observacio."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
}

function afegirPagament($idEquip,$isPagat){
	conecta();
	$sql = "UPDATE equip SET isPagat='".$isPagat."' WHERE equip.idEquip = '".$idEquip."' ";
	mysql_query ( $sql );
}

function getObservacioEquip($idEquip){
	conecta();
	$dades = array();
	//$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
	$queryEquip = mysql_query ( 'SELECT equip.observacions from equip
					WHERE equip.idEquip = "' . $idEquip . '" '  );
	while ($desti = mysql_fetch_assoc($queryEquip)){
	    		array_push($dades, $desti);
	    	}
	return $dades;
}

function getEquipsByidDivisio($idDivisio){
	conecta();
	$dades = array();
	$queryEquip = mysql_query ( 'SELECT equip.idEquip, equip.nomEquip, jugador.nomJugador, jugador.cognomsJugador, jugador.email, jugador.telefon, equip.preferenciaPartit, equip.observacions from equip 
	inner join divisio on divisio.idDivisio = equip.idDivisio left join jugador on jugador.idEquip = equip.idEquip 
	WHERE divisio.idDivisio = "' . $idDivisio. '"  and equip.actiu = "ACT" group by equip.nomEquip order by equip.nomEquip ASC');
	while ($desti = mysql_fetch_assoc($queryEquip)){
	    	echo "<tr><td>".$desti['idEquip']."</td><td><a href='mostraDadesEquip.php?idEquip=".$desti['idEquip']."'>".$desti['nomEquip']."</a></td><td>".$desti['nomJugador']." ".$desti['cognomsJugador']."</td><td>".$desti['email']."</td><td>".$desti['telefon']."</td><td>".$desti['preferenciaPartit']."</td><td>".$desti['observacions']."</td></tr>";
	   
	}
	return $dades;
}

function getEquipsByidDivisioPista($idDivisio){
	conecta();
	$dades = array();
	$queryEquip = mysql_query ( 'SELECT equip.idEquip, equip.nomEquip, jugador.nomJugador, jugador.cognomsJugador, jugador.email, jugador.telefon, equip.preferenciaPartit, pistaJoc.direccioPista,pistaJoc.horariPista,pistaJoc.poblacioPista, equip.observacions from equip 
	inner join divisio on divisio.idDivisio = equip.idDivisio left join jugador on jugador.idEquip = equip.idEquip left join pistaJoc on pistaJoc.idEquip = equip.idEquip
	WHERE divisio.idDivisio = "' . $idDivisio. '" and equip.actiu = "ACT" and jugador.actiu = "ACT" and jugador.isPrimerCapita = 1 group by equip.nomEquip order by equip.nomEquip ASC');
	while ($desti = mysql_fetch_assoc($queryEquip)){
	if ($desti['direccioPista']==''){$direccio = '-';}else{$direccio = $desti['direccioPista'];}
	if ($desti['horariPista']==''){$hora = '-';}else{$hora=$desti['horariPista'];}
	if ($desti['poblacioPista']==''){$poblacio = '-';}else{$poblacio = $desti['poblacioPista'];}
	    	echo "<tr><td>".$desti['idEquip']."</td><td><a href='mostraDadesEquip.php?idEquip=".$desti['idEquip']."'>".$desti['nomEquip']."</a></td><td>".$desti['nomJugador']." ".$desti['cognomsJugador']."</td><td>".$desti['email']."</td><td>".$desti['telefon']."</td><td>".$desti['preferenciaPartit']."</td><td>".$direccio."</td><td>".$hora."</td><td>".$poblacio."</td><td>".$desti['observacions']."</td></tr>";
	   
	}
	return $dades;
}

function getIdDivsiobyIdEquip($idEquip){
	conecta ();
	$queryEquip = mysql_query ( 'select divisio.idDivisio from divisio left join equip on equip.idDivisio = divisio.idDivisio where equip.idEquip="' . ($idEquip) . '" and equip.actiu = "ACT" limit 1 '  );
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row['idDivisio'];
	
}

function getIdEquipbyNomEquip($nomEquip){
	conecta ();
	$queryEquip = mysql_query ( 'select equip.idEquip from equip  where equip.nomEquip="' . ($nomEquip) . '" and equip.actiu = "ACT" limit 1 '  );
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row['idEquip'];
}

function getIdPistaJocbyIdEquip($idEquip){
	conecta ();
	$queryEquip = mysql_query ( 'select pistaJoc.idPistaJoc from pistaJoc left join equip on equip.idEquip = pistaJoc.idEquip where equip.idEquip = "' . ($idEquip) . '" and equip.actiu = "ACT" limit 1 '  );
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row['idPistaJoc'];
	
}

function getPreferenciaByIdEquip($idEquip){
        conecta ();
	$queryEquip = mysql_query ( 'select equip.preferenciaPartit from equip where equip.idEquip = "' . ($idEquip) . '" and equip.actiu = "ACT" limit 1 '  );
	$row = mysql_fetch_assoc ( $queryEquip );
	return $row['preferenciaPartit'];
}

function getPagament($idEquip){
	conecta();
	$inscripcio = '';$esportivitat = 'NO';$pistaPropia = 'NO';$preferencia = 'NO';
	$idDivisio = getIdDivsiobyIdEquip($idEquip);
	$pistaJoc = getIdPistaJocbyIdEquip($idEquip);
	$pref = getPreferenciaByIdEquip($idEquip);
	if ($idDivisio == 1 || $idDivisio == 2 || $idDivisio == 5){
		$inscripcio = 890;
	}elseif ($idDivisio == 3 || $idDivisio == 4){
		if ($pistaJoc != '' || $pistaJoc != 0){
			$inscripcio = 595;
			$pistaPropia = 'SI';
		}else{
			$inscripcio = 720;
		}
	}else{
		$inscripcio = 0;
	}
	if ($idEquip == 55 || $idEquip == 38 || $idEquip == 37 || $idEquip == 48){
		$esportivitat = 'SI (25%)';
		$inscripcio -= ($inscripcio * 0.25);
	/*}elseif ($idEquip == 31 || $idEquip == 19 || $idEquip == 43 || $idEquip == 63){
		$esportivitat = 'SI (12.5%)';
		$inscripcio -= ($inscripcio * 0.125);*/
	}
	if ($pref != NULL || $pref != ''){
	     $preferencia = 'SI (60€)';
	}
	$dades = array();
	$queryEquip = mysql_query ('select equip.idEquip, equip.nomEquip, count(jugador.idJugador) as Jugadors, (count(jugador.idJugador)*40) as totalFitxes, ((count(jugador.idJugador)*40)+'.$inscripcio.') as totalPagar, equip.isPagat from equip 
	inner join divisio on divisio.idDivisio = equip.idDivisio 
	inner join jugador on jugador.idEquip = equip.idEquip 
	WHERE divisio.idDivisio = "' . $idDivisio. '" and equip.idEquip = "' . $idEquip. '" and equip.actiu = "ACT" and jugador.actiu = "ACT" ');
        while ($desti = mysql_fetch_assoc($queryEquip)){
       		$idEquip = $desti['idEquip'];
		$pagament = $desti['isPagat'];
        	if ($pagament == 0){
			$isPagat = 'NO';
			$pagament = "<a href='../app/afegirPagament.php?idEquip=$idEquip&&isPagat=1'><button type='button' class='btn btn-success'>PAGAR</button></a>";
		}else{
			$isPagat = 'SI';
			$pagament = "<a href='../app/afegirPagament.php?idEquip=$idEquip&&isPagat=0'><button type='button' class='btn btn-danger'>TREURE PAGAMENT</button></a>";
		}
		if($_SESSION['USUARIO']['rol'] != 1){$pagament = '';}
		if($preferencia != 'NO'){$desti['totalPagar'] += 60;}
		
		if($idEquip == 52 && $_SESSION['USUARIO']['rol'] == 1){
			$desti['Jugadors'] -= 2;
			$desti['totalFitxes'] = $desti['Jugadors'] * 40;
			$desti['totalPagar'] -= 80;
		}
		    
	    	echo "<tr><td>".$desti['nomEquip']."</td><td>".$desti['Jugadors']."</td><td>".$desti['totalFitxes']." .-</td><td>".$pistaPropia."</td><td>".$esportivitat."</td><td>".$preferencia."</td><td>".$inscripcio." .-</td><td>".$desti['totalPagar']." .-</td><td>".$isPagat."</td><td>".$pagament."</td></tr>";
	   
	}
	return $dades;
}

function getDadesRebut($idEquip){
	conecta();
	$inscripcio = '';$esportivitat = 'NO';$pistaPropia = 'NO';
	$idDivisio = getIdDivsiobyIdEquip($idEquip);
	$pistaJoc = getIdPistaJocbyIdEquip($idEquip);
	$pref = getPreferenciaByIdEquip($idEquip);
	if ($idDivisio == 1 || $idDivisio == 2 || $idDivisio == 5){
		$inscripcio = 890;
	}elseif ($idDivisio == 3 || $idDivisio == 4){
		if ($pistaJoc != '' || $pistaJoc != 0){
			$inscripcio = 595;
			$pistaPropia = 'SI';
		}else{
			$inscripcio = 690;
		}
	}else{
		$inscripcio = 0;
	}
	if ($idEquip == 55 || $idEquip == 38 || $idEquip == 37 || $idEquip == 48){
		$esportivitat = 'SI (25%)';
		$inscripcio -= ($inscripcio * 0.25);
	/*}elseif ($idEquip == 31 || $idEquip == 19 || $idEquip == 43 || $idEquip == 63){
		$esportivitat = 'SI (12.5%)';
		$inscripcio -= ($inscripcio * 0.125);*/
	}
	if ($pref != NULL || $pref != ''){
	     $preferencia = 'SI (60€)';
	}
	$dades = array();
	//$idUser = mysql_fetch_row ( mysql_query ( 'select usuari.idUser from usuari where usuari.nomUser ="' . ($user) . '" ' ) );
	$queryEquip = mysql_query ( 'select equip.idEquip, equip.nomEquip, count(jugador.idJugador) as Jugadors, (count(jugador.idJugador)*40) as totalFitxes, ((count(jugador.idJugador)*40)+'.$inscripcio.') as totalPagar, equip.isPagat from equip 
	inner join divisio on divisio.idDivisio = equip.idDivisio 
	inner join jugador on jugador.idEquip = equip.idEquip 
	WHERE divisio.idDivisio = "' . $idDivisio. '" and equip.idEquip = "' . $idEquip. '" and equip.actiu = "ACT"  and jugador.actiu = "ACT" ' );
	while ($desti = mysql_fetch_assoc($queryEquip)){
	    		array_push($dades, $desti);
	    	}
	return $dades;
}

function getDadesFitxa($idJugador){
	conecta();
	$dades = array();
	$queryJugador = mysql_query('SELECT jugador.nomJugador, jugador.cognomsJugador, jugador.foto, jugador.dni,jugador.dataNeixament, jugador.telefon,jugador.poblacio, equip.nomEquip ,divisio.nomDivisio 
					from jugador
					inner join equip on equip.idEquip = jugador.idEquip
					inner join divisio on divisio.idDivisio = equip.idDivisio
					WHERE jugador.idJugador = "' . $idJugador. '" and jugador.actiu = "ACT" '  );
	/*while ($desti = mysql_fetch_assoc($queryEquip)){
		array_push($dades, $desti);
	}*/
	$row = mysql_fetch_assoc ( $queryJugador );
	return $row;
}
?>
                            