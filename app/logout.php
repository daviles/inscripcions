<?php @session_start();
/* 
 * Cierra la sesión como usuario validado
 */
include('config.ini.php');
include('login.lib.php'); //incluimos las funciones
if ($_SESSION['USUARIO']['rol'] == 1){
	$correu = "davevudu@gmail.com";
	$asunto = "Tancament sessio ".$_SESSION['USUARIO']['nomUser'];
	$ip = getRealIP();
	$hora= date ("h:i:s");
	$fecha= date ("j/n/Y");
	$cos = "L'usuari : ".$_SESSION['USUARIO']['nomUser']." ha tancat sessio amb IP ".$ip." en fecha : ".$fecha." y hora : ".$hora;
	$resultado = mail($correu,$asunto,$cos);
	if ($resultado) {
		echo "Correo enviado correctamente";
	}else{
		echo "El correo no se ha enviado correctamente";
	}
	$file = fopen("sessions.txt", "a");
	fwrite($file, $cos . PHP_EOL);
	fclose($file);
}
logout(); //vacia la session del usuario actual
header('Location: ../public/index.php'); //saltamos a login.php

?>
