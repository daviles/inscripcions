/*DROP DATABASE IF EXISTS balles;*/
/*CREATE DATABASE balles;*/
USE nhotqobg_balles;

CREATE TABLE IF NOT EXISTS usuari (
    	idUser INT(10) NOT NULL auto_increment,
		nomUser VARCHAR(20) NOT NULL,
    	pass VARCHAR(15) NOT NULL,	
		idEquip INT(10),
		rol INT(1) DEFAULT 0,
    	PRIMARY KEY (idUser)
)  ENGINE=INNODB;	

CREATE TABLE IF NOT EXISTS jugador (
			idJugador INT(10) NOT NULL auto_increment,
			nomJugador VARCHAR(50) NOT NULL,
			cognomsJugador VARCHAR(75) NOT NULL,
			dni VARCHAR(9) NOT NULL,
			dataNeixament VARCHAR(10) NOT NULL,
			poblacio VARCHAR(100),
			email VARCHAR(50),
			telefon int(9),
			dorsal int(2),
			isPrimerCapita int(1) DEFAULT 0,
			isSegonCapita INT(1) DEFAULT 0,
			foto VARCHAR(100),
			dniAdj VARCHAR(100),
			idEquip INT(10),
			PRIMARY KEY (idJugador)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS equip (
			idEquip INT(10) NOT NULL auto_increment,
			nomEquip VARCHAR(100) NOT NULL,
			preferenciaPartit VARCHAR(255),
			colorSamarreta VARCHAR(50),
			colorPantalo VARCHAR(50),
			idDivisio INT(10),
			PRIMARY KEY(idEquip)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS pistaJoc (
			idPistaJoc INT(10) NOT NULL auto_increment,
			nomPista VARCHAR(100) NOT NULL,
			horariPista VARCHAR(10),
			poblacioPista VARCHAR(50),
			direccioPista VARCHAR(200),
			idEquip INT(10),
			PRIMARY KEY(idPistaJoc)
) ENGINE=INNODB;

CREATE TABLE IF NOT EXISTS divisio(
			idDivisio INT(10) NOT NULL auto_increment,
			nomDivisio VARCHAR (200),
			PRIMARY KEY(idDivisio)
) ENGINE=INNODB;

# ************ Creamos las relaciones ************
ALTER TABLE usuari
	ADD FOREIGN KEY (idEquip) REFERENCES equip(idEquip);

ALTER TABLE jugador
	ADD FOREIGN KEY (idEquip) REFERENCES equip(idEquip);

ALTER TABLE equip
	ADD FOREIGN KEY (idDivisio) REFERENCES divisio(idDivisio);

ALTER TABLE pistaJoc
	ADD FOREIGN KEY (idEquip) REFERENCES equip(idEquip);


# ************ FEM INSERTS ************
INSERT INTO equip(nomEquip) VALUES ("Balles");
INSERT INTO usuari(nomUser,pass,idEquip,rol) VALUES ("david","vivelavida1978",1,1);
INSERT INTO usuari(nomUser,pass,idEquip,rol) VALUES ("txell","Whvelico2007",1,1);
INSERT INTO usuari(nomUser,pass,idEquip,rol) VALUES ("maria","Whvelico2007",1,1);
INSERT INTO usuari(nomUser,pass,idEquip,rol) VALUES ("jessica","Whvelico2007",1,1);
INSERT INTO usuari(nomUser,pass,idEquip,rol) VALUES ("sergi","Whvelico2007",1,1);

INSERT INTO divisio(nomDivisio) VALUES ("Primera Masculina");
INSERT INTO divisio(nomDivisio) VALUES ("Segona Masculina");
INSERT INTO divisio(nomDivisio) VALUES ("Primera Femenina");
INSERT INTO divisio(nomDivisio) VALUES ("Segona Femenina");



	
